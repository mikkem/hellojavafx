package Valutakalkulator;

import java.util.Hashtable;

public class ValutaKalkulator {
    
    private Hashtable<String, Double> valuta = 
            new Hashtable<String, Double>(); 
	
	public void initialize() {
		valuta.put("Dollar", 1.0);
		valuta.put("Kr", 0.11);
		valuta.put("Euro", 0.84);
	}
	public double calcuate(String kurs1, String kurs2) {
		if(valuta.get(kurs2) == null || valuta.get(kurs1)== null) {
			return 0.0;
		}
		return valuta.get(kurs2)/valuta.get(kurs1);	
	}
}
    


