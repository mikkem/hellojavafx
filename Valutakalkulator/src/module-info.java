module Valutakalkulatorpp {
	requires javafx.fxml;
	requires transitive javafx.graphics;
	requires javafx.controls;
	
	exports Valutakalkulator;

	opens Valutakalkulator to javafx.fxml;
}