package Valutakalkulator;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class AppController {

   @FXML
	private TextField input, output; 
	
	@FXML
	private ComboBox<String> comboBox, comboBox1; 
	
	private final ValutaKalkulator vk = new ValutaKalkulator();
	@FXML
	public void initialize() {
		vk.initialize();
		
	    comboBox.getItems().removeAll(comboBox.getItems());
	    comboBox.getItems().addAll("Kr", "Dollar", "Euro");
	    comboBox.getSelectionModel().select("Dollar");
	    
	    comboBox1.getItems().removeAll(comboBox1.getItems());
	    comboBox1.getItems().addAll("Kr", "Dollar", "Euro");
	    comboBox1.getSelectionModel().select("Kr");
    }

    private double getInput() {
        String txt=input.getText();
		if(txt.isEmpty())
			return 0.0;
        try {  
            double nr = Double.parseDouble(txt); 
            return nr;
        } catch(NumberFormatException e){  
            return 0.0;  
        }  
	}
	
	@FXML
    private void calcu() {
	 
	 
	 Double kurs = getInput();
	kurs = ((double) (Math.round(1000*kurs*vk.calcuate(comboBox.getValue().toString(), comboBox1.getValue().toString())
			 )))/1000;
	 
	 this.output.setText(Double.toString(kurs));
		
    }
}
